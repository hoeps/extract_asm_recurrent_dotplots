# Whoeps, 23rd July 2021


configfile: "./Snake.config.json"

# Possible genomes: hg38, chm13. Paths in Snake.config.json!
GENOMES = ['hg38']


# Get input fastas. The 'dummy' I use so that re results are in the right format. 
# Basically very stupid workaround for something :) 
# Option A: hifiasm
#SAMPLES, dummy = glob_wildcards("/home/hoeps/hifiasm_v13dev/{sample}.fast{dummy}")
# Option B: HPRC Pangenome assemblies:
SAMPLES, dummy = glob_wildcards("/scratch/hoeps/hprc_assemblies/{sample}.f1_assembly_v2_genban{dummy}.fa")

# Option B and C: ccs or clr assemblies (but they are considered inferior to option A)
#SAMPLES, dummy = glob_wildcards("/g/korbel/hoeps/lab/ccs_fastas/{sample}.fast{dummy}")
#SAMPLES, dummy = glob_wildcards("/g/korbel/hoeps/lab/assemblies/fastas/clr/{sample}.{dummy}asta")

SAMPLES = sorted(set(SAMPLES))

# Define outputs
inv_samples = expand("processed/{genome}/paf/{sample}.paf",  genome=GENOMES, sample=SAMPLES)
htmls = expand("processed/{genome}/dotplot/{sample}.html",  genome=GENOMES, sample=SAMPLES)
anchors = expand("processed/roi_{genome}_anchor.fa", genome=GENOMES)

localrules: all, get_bedfile, get_hg38_fasta, make_one_chr, get_dotplot


rule all:
    input: 
        inv_samples, 
        htmls,
        anchors
        

rule get_beds:
    """
    Take in a bedfile with one entry, namely the region of interest. 
    Return a fasta file both of the region, and of the anchor region.
    Where and how long the anchor should be is defined in extract_ref_fasta.sh.
    """
    input: 
        "data/roi_{genome}.bed"
    output:
        fa = "processed/roi_{genome}.fa",
        anchor = "processed/roi_{genome}_anchor.fa"

    params:
        ref = lambda w: config['{}_fasta'.format(w.genome)]
    shell:
        """
        bash bash/extract_ref_fasta.sh \
                  {input} \
                  {params.ref} \
                  {output.fa} \
                  {output.anchor}
        """

rule get_pafs:
    """
    Given a sequence anchor (fasta), find that sequence back in the assembly, and 
    return the region in the assembly as a fasta. 
    ADDITIONALLY, return a .paf assembly of the two regions (... to be used for the
    dotplot in the next step)
    """
    input:
        inv_fa = "processed/roi_{genome}.fa",
        anchor_fasta = "processed/roi_{genome}_anchor.fa", 
    output:
        paf = "processed/{genome}/paf/{sample}.paf",
    params:
        anchor_bed = "processed/roi_{genome}_{sample}_anchor.bed"

    shell:
        """
        bash bash/extract_inv.sh \
                   {input.inv_fa} \
                   {input.anchor_fasta} \
                   /home/hoeps/hifiasm_v13dev/{wildcards.sample}.fasta \
                   {params.anchor_bed} \
                   {output.paf}
        """


rule get_dotplots:
    """
    Use the dotplotly package to turn visualize .paf files as dotplots. 
    """
    input: 
        "processed/{genome}/paf/{sample}.paf"
    output:
        "processed/{genome}/dotplot/{sample}.html"
    shell:
        """
        bash bash/make_dotplot.sh \
                 {input} \
                 {output}
        """
