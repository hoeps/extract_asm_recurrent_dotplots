#!/bin/bash
set -euo pipefail

alignmentfile=$1
outfile=$2

# Invoke doplotly
Rscript bash/dotPlotly/pafCoordsDotPlotly.R -i ${alignmentfile} -o /g/korbel/hoeps/lab/3q29_dotplots/${outfile} -m 10000 -q 10 -p 10 -l -p 10 

