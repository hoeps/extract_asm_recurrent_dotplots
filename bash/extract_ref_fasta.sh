#!/bin/bash

inv_bedfile=$1
ref=$2
outfile=$3 
outfile_anchor=$4 

# Go into the reference fasta, extract sequence of interest ...
bedtools getfasta -fi ${ref} -bed ${inv_bedfile} > ${outfile}
# ... and an anchor. In this case, 150kb upstream of the start of region of interest.
bedtools getfasta -fi ${ref} -bed <(awk '{FS=OFS="\t"} {print $1,$2-150000,$2}' ${inv_bedfile}) > ${outfile_anchor}
