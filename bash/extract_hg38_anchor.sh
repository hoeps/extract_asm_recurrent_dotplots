# WOLFY COMMENT 30TH SEP 2021
# THE FOLLOWING SCRIPT IS UNUSED BY THE PIPELINE. 
# I'M JUST KEEPING IT HERE BECAUSE I'M NOT SURE WHY 
# I USED TO NEED IT. 

#!/bin/bash
set -euo pipefail 

inv_bedfile=$1
invname=$2
anchor_upstream=$3
anchor_len=$4

ref=$'/g/korbel/hoeps/projects/huminvs/data/hg38/ref_chr_1_22XY.fa'
# Upstream bed hg38

mkdir -p /scratch/hoeps/inv_refinement3/${invname}/anchors_hg38

echo 
awk -v u=$anchor_upstream -v l=$anchor_len '{FS=OFS="\t"} {print $1,$2-u,($2-u)+l}' ${inv_bedfile} > /scratch/hoeps/inv_refinement3/${invname}/anchors_hg38/${invname}_hg38_anchor.bed #works
# upstream fasta hg38
bedtools getfasta -fi ${ref} -bed /scratch/hoeps/inv_refinement3/${invname}/anchors_hg38/${invname}_hg38_anchor.bed > /scratch/hoeps/inv_refinement3/${invname}/anchors_hg38/${invname}_hg38_anchor.fa #works
