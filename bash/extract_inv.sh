#!/bin/bash
set -euo pipefail 
inv_fasta=$1
anchor_fasta=$2
assembly_fasta=$3
anchor_bed=$4
outfilename=$5


minimap2=$'/g/korbel/hoeps/programs/paftools/minimap2/minimap2'

# Index inv fasta
samtools faidx ${anchor_fasta}

# Find assembly anchor and extend it to cover full sequence
awk '{FS=OFS="\t"} ($8>1000000) {print $6,$8-1000000,$8+1000000} ($8<1000000) {print $6,0,$8+1000000}' <(${minimap2} -c ${assembly_fasta} ${anchor_fasta}) > ${anchor_bed}
echo done

# Turn bed into .region file, which faidx likes as input
awk '{FS=OFS="\t"} (NR==1){print $1":"$2"-"$3} ' ${anchor_bed} > ${anchor_bed}.region

samtools faidx ${assembly_fasta} -r ${anchor_bed}.region > ${anchor_bed}.region.fa

# Find fasta in assembly, print alignment

#${minimap2} -D -P -z 2000,200 -s 200000000000000 ${inv_fasta} ${anchor_bed}.region.fa > ${outfilename}

# This here works quite well. Gives nice plots
# ${minimap2} -z 2000,200 -s 40 -p 0.01, -N 50000 ${inv_fasta} ${anchor_bed}.region.fa > ${outfilename}

# Here is one that presumably creates more lines
${minimap2} -z 2000,200 -s 40 -p 0.01, -N 50000 ${inv_fasta} ${anchor_bed}.region.fa > ${outfilename}

### FROM HERE ON: TRASH AND OLD STUFF ###

# find new coords h1 and h2
#awk '{FS=OFS="\t"} {print $6,$8,$9}' <(${minimap2} -c ../*${samplename}*h1*.fasta /scratch/hoeps/inv_refinement3/${invname}/anchors_hg38/${invname}_hg38_anchor.fa) > /scratch/hoeps/inv_refinement3/${invname}/asm_anchors/${invname}_${samplename}_h1_anchor.bed 
#awk '{FS=OFS="\t"} {print $6,$8,$9}' <(${minimap2} -c ../*${samplename}*h2*.fasta /scratch/hoeps/inv_refinement3/${invname}/anchors_hg38/${invname}_hg38_anchor.fa) > /scratch/hoeps/inv_refinement3/${invname}/asm_anchors/${invname}_${samplename}_h2_anchor.bed
#echo 3
#awk '{FS=OFS="\t"} ($2 < 200000){$2=200000} {print $1,$2-200000,$2+200000}' /scratch/hoeps/inv_refinement3/${invname}/asm_anchors/${invname}_${samplename}_h1_anchor.bed > /scratch/hoeps/inv_refinement3/${invname}/asm_pm200k/inv${invname}_${samplename}_h1_pm200kb.bed #works
#awk '{FS=OFS="\t"} ($2 < 200000){$2=200000} {print $1,$2-200000,$2+200000}' /scratch/hoeps/inv_refinement3/${invname}/asm_anchors/${invname}_${samplename}_h2_anchor.bed > /scratch/hoeps/inv_refinement3/${invname}/asm_pm200k/inv${invname}_${samplename}_h2_pm200kb.bed
##
#echo 4
#awk '{FS=OFS="\t"} (NR==1){print $1":"$2"-"$3} ' /scratch/hoeps/inv_refinement3/${invname}/asm_pm200k/inv${invname}_${samplename}_h1_pm200kb.bed > /scratch/hoeps/inv_refinement3/${invname}/asm_pm200k/inv${invname}_${samplename}_h1_pm200kb.region
#awk '{FS=OFS="\t"} (NR==1){print $1":"$2"-"$3} ' /scratch/hoeps/inv_refinement3/${invname}/asm_pm200k/inv${invname}_${samplename}_h2_pm200kb.bed > /scratch/hoeps/inv_refinement3/${invname}/asm_pm200k/inv${invname}_${samplename}_h2_pm200kb.region
##
#echo 5
#s

#echo 6
# make pafs
#${minimap2} -c -DP  /scratch/hoeps/inv_refinement3/${invname}/asm_pm200k/${invname}_${samplename}_h1.fa /scratch/hoeps/inv_refinement3/${invname}/hg38/${invname}_hg38_pm200kb.fa > /scratch/hoeps/inv_refinement3/${invname}/paf/${invname}_${samplename}_h1.paf
#${minimap2} -c -DP  /scratch/hoeps/inv_refinement3/${invname}/asm_pm200k/${invname}_${samplename}_h2.fa /scratch/hoeps/inv_refinement3/${invname}/hg38/${invname}_hg38_pm200kb.fa > /scratch/hoeps/inv_refinement3/${invname}/paf/${invname}_${samplename}_h2.paf

#echo 7
#./dotPlotly/pafCoordsDotPlotly.R -i ${invname}/paf/${invname}_${samplename}_h1.paf -o /g/korbel/hoeps/lab/assemblies/new_invextracts/${invname}/dotplots/${invname}_${samplename}_h1 -m 11 -q 11 -l -p 10 -j beds/${invname}.bed
#./dotPlotly/pafCoordsDotPlotly.R -i ${invname}/paf/${invname}_${samplename}_h2.paf -o /g/korbel/hoeps/lab/assemblies/new_invextracts/${invname}/dotplots/${invname}_${samplename}_h2 -m 11 -q 11 -l -p 10 -j beds/${invname}.bed
